"""Todo models
"""
from enum import Enum

from sqlalchemy import Column, Integer, Boolean, Text, Date
from sqlalchemy.ext.declarative import declarative_base

from datetime import date

Base = declarative_base()


class Tags(Enum):
    study = "Учёба"
    personal = "Личное"
    plans = "Планы"
    others = "Разное"


class Todo(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(Text)
    details = Column(Text)
    completed = Column(Boolean, default=False)
    tag = Column(Text, default=Tags.plans)
    data = Column(Text)

    def __repr__(self):
        return f'<Todo {self.id}>'
