"""Main of todo app
"""
import math

from loguru import logger
from fastapi import FastAPI, Request, Depends, Form, status, HTTPException
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles
from database import init_db, get_db, Session

import models

from sqlalchemy import func

import datetime
import _strptime

init_db()

# pylint: disable=invalid-name
templates = Jinja2Templates(directory="templates")

app = FastAPI()

logger = logger.opt(colors=True)
# pylint: enable=invalid-name

app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
async def home(request: Request,
               title: str = None,
               details: str = None,
               data: str = None,
               database: Session = Depends(get_db),
               limit: int = 10,
               skip: int = 0):
    """ Главная страница с ToDo лист-ом
    """
    logger.info("In home")
    todo_count = database.query(models.Todo).count() #количество ToDo
    count_list = count_tag(database) # кортеж, в котором каждый элемент представляет собой также кортеж из двух элементов (название тега - кол-во данных задача с этим тегом)
    pages = math.ceil(todo_count / limit) # кол-во страниц с ToDo
    todos = database.query(models.Todo).order_by(models.Todo.id.desc()).offset(skip * limit).limit(limit)
    return templates.TemplateResponse("index.html", {"request": request, "todo_count": todo_count,
                                                     "todos": todos, "count_tag": count_list,
                                                     "title": title, "details": details,
                                                     "page": skip, "pages": pages, "limit": limit, "data": data
                                                     })
@app.post("/add")
async def todo_add(request: Request,
                   title: str = Form(default=""),
                   details: str = Form(default=""),
                   tag: str = Form(default="Планы"),
                   data: str = Form(default=""),
                   database: Session = Depends(get_db)):
    """Добавить новое ToDo
    """
    if title == "":
        logger.warning("Поле с названием - пустое!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}',
                                status_code=status.HTTP_303_SEE_OTHER)
    if len(title) > 500:
        logger.warning("В имени ToDo больше 500 символов!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}',
                                status_code=status.HTTP_303_SEE_OTHER)
    try:
        datetime.datetime.strptime(data, '%Y-%m-%d')
    except ValueError:
        logger.warning("Неверный формат даты!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}'
                                    f'&details={data}',
                                status_code=status.HTTP_303_SEE_OTHER)
    todo = models.Todo(title=title, details=details, tag=tag, data=data)
    logger.info(f"Creating todo: {todo.title}")
    database.add(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/done/{todo_id}")
async def done(todo_id: int,
               database: Session = Depends(get_db)):
    """ Для кнопок выполнено/не выполнено
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    todo.completed = not todo.completed
    database.add(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for('home'), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/edit/{todo_id}")
async def todo_get(request: Request,
                   todo_id: int,
                   database: Session = Depends(get_db)):
    """ Get Edit ToDo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    logger.info(f"Getting todo: {todo}")
    logger.info(f"{todo.tag}")
    return templates.TemplateResponse("edit.html", {"request": request, "todo": todo, "tags": models.Tags})

@app.post("/edit/{todo_id}")
async def todo_edit(
                    request: Request,
                    todo_id: int,
                    title: str = Form(""),
                    completed: bool = Form(False),
                    tag: str = Form("Планы"),
                    details: str = Form(""),
                    data: str = Form(""),
                    database: Session = Depends(get_db)):
    """ Изменить ToDo
    """
    if title == "":
        logger.warning("Поле с названием - пустое!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}',
                                status_code=status.HTTP_303_SEE_OTHER)
    if len(title) > 500:
        logger.warning("В имени ToDo больше 500 символов!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}',
                                status_code=status.HTTP_303_SEE_OTHER)
    try:
        datetime.datetime.strptime(data, '%Y-%m-%d')
    except ValueError:
        logger.warning("Неверный формат даты!")
        return RedirectResponse(url=f'{app.url_path_for("home")}'
                                    f'?title={title}'
                                    f'&details={details}'
                                    f'&details={data}',
                                status_code=status.HTTP_303_SEE_OTHER)
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    logger.info(f"Editting todo: {todo.title}")
    todo.title = title
    todo.details = details
    todo.completed = completed
    todo.tag = tag
    todo.data = data
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)

@app.post("/{todo_id}")
async def todo_edit_completed(
        todo_id: int,
        database: Session = Depends(get_db)):
    """Edit completed in main page
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    todo.completed = not todo.completed
    database.commit()

    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/delete/{todo_id}")
async def todo_delete(request: Request,
                      todo_id: int,
                      database: Session = Depends(get_db)):
    """Delete todo
    """
    todo = database.query(models.Todo).filter(models.Todo.id == todo_id).first()
    if todo is None:
        logger.warning("Does not exist")
        return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    logger.info(f"Deleting todo: {todo}")
    database.delete(todo)
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)

@app.get("/todo_delete_all")
async def todo_delete_all(database: Session = Depends(get_db)):
    """Delete all todos
    """
    todo = database.query(models.Todo).all()
    count = 0
    for i in todo:
        if 0 <= count <= 100000:
            database.delete(i)
            database.commit()
        count += 1
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)

def count_tag(database: Session = Depends(get_db)): # фунцкия для подсчета количества тегов
    """Ф-ия подсчитывает кол-во тегов
    """
    tags_count = database.query(models.Todo.tag,func.count()).group_by(models.Todo.tag).all()
    return tags_count

@app.get("/done_all")
async def done_all(database: Session = Depends(get_db)):
    """ Для кнопок выполнено/не выполнено
    """
    todo_count = database.query(models.Todo).count()
    logger.info(f"Количество ToDo: {todo_count}")
    for i in range(1, todo_count+1):
        todo = database.query(models.Todo).filter(models.Todo.id == i).first()
        if todo.completed == False:
            todo.completed = not todo.completed
        i += 1
    database.commit()
    return RedirectResponse(url=app.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
