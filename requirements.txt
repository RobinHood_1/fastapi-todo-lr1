fastapi==0.86.0
loguru==0.6.0
python-multipart==0.0.5
pydantic==1.10.2
SQLAlchemy==1.4.41
uvicorn==0.19.0
Jinja2==3.1.2
mpmath==1.1.0
