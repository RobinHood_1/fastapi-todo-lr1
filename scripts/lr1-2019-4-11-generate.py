import random
import requests
import string
from sys import argv

def generate_random_string(length):
    letters = string.ascii_letters
    rand_string = ''.join(random.choice(letters) for i in range(length))
    return rand_string
def main():
    url="http://localhost:80/add"
    for i in range(20):
        new_task = {"title": generate_random_string(random.randint(1, 10))}
        r = requests.post(url, data=new_task)

if __name__ == "__main__":
    main()
