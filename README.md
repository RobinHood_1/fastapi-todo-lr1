# fastapi-todo-lr1

### Процесс запуска приложения: 

* Перед началом работы с Docker необходимо сделать git-clone проекта с gitlab:
```bash
git clone git@gitlab.com:RobinHood_1/fastapi-todo-lr1.git
cd fastapi-todo-lr1
```
* Запуск процесса создания Docker-образа с использованием прокси-сервера:
```bash
sudo docker build --build-arg PROXY=http://login:password@192.168.1.12:3128 -t lr1-2019-4-11 .
```
* Запуск процесса создания Docker-образа без использования прокси-сервера:
```bash
sudo docker build -t lr1-2019-4-11 .
```
* Запуск обычной версии Docker-контейнера:
```bash
sudo docker run --rm -p 80:80 lr1-2019-4-11
```
* Запуск dev-версии Docker-контейнера с сохранением данных в локальную базу:
```bash
sudo docker run --rm -p 80:80 -v "${PWD}/data/":/code/data/ lr1-2019-4-11
```
* Запуск dev-версии Docker-контейнера с автоматически пробрасываемым кодом в контейнер:
```bash
sudo docker run --rm -p 80:80 -v "${PWD}/app":/code/app -v "${PWD}/data/db.sqlite":/code/data/db.sqlite lr1-2019-4-11
```
* Если firewall выдает "The requested URL could not be retrieved" нужно изменить 0.0.0.0 на localhost.

### Запуск скрипта генерации 20-ти случайных тудушек:

* Из папки fastapi-todo-lr1 спускаемся в папку scripts:
```bash
cd ./scripts
```
* Создаём новый Docker-образ для работы скрипта: 
```bash
sudo docker build -t lr1-2019-4-11-generate .
 ```
* Запускаем новый Docker-контейнер (работает, когда запущена ToDo):
```bash
sudo docker run --rm --network=host lr1-2019-4-11-generate
```




